from lga_experiment import Experiment
from lga_params import *

MAXR=128
MAXC=256
class FlowExpt(Experiment):
    def __init__(self, *args):
        Experiment.__init__(self, *args)
        self.name = "Test Flow"

    def setup(self):
        self.currentStatFn = self.stat_density
        self.loadBlankLGA((MAXR,MAXC))
        self.setup_boundaries()
        self.visualizer.lgaChanged()
        self.SRCVPARTICLES = WW

    def start(self):
        ENDTIME = 1000
        PRINTINTERVAL = 100
        # Define which function to use for the stats
        self.currentStatFn = self.stat_density
        self.loadBlankLGA((MAXR,MAXC))
        self.setup_particles()
        self.setup_boundaries()
        self.visualizer.lgaChanged()

        print "Running until ", ENDTIME
        while self.lga.t < ENDTIME:
            self.step()
            if (self.lga.t % PRINTINTERVAL == 0):
                print self.lga.t
        myax = self.visualizer.ui_plot_stats('Funnel demon test. ') # Show graph of stats

        return


    def setup_particles(self):
        # Put in a bunch of random particles in a rectangular shape
        nr, nc = self.lga.A.shape
        print "Setting up particles L! nr=",nr, " nc=",nc
        rectLsml = (20,4,-4+2+nr/2,-4+nc/2) # shrink the square so that the particles don't overlap walls
        print "rectLsml = ", rectLsml
        self.lga.RectNFill(0.5,rectLsml) #Fill Left side with exactly p*vol(rect) particles
        return


    # SRC:  source of particles heading EE
    # SRCA: source of particles heading ALL directions
    # SRCV: source of particles heading in a direction specified by _V_ariable SRCVPARTICLES

    def setup_boundaries(self):
        # Set up container / boundaries / world
        self.lga.DrawWalls() # This makes container
        
        self.lga.SRCVPARTICLES = WW
        nr, nc = self.lga.A.shape
        mr = nr/2 # middle row
        mc = nc/2 # middle col
        self.lga.DrawLine(4,4,nr-4,4,SRC) # line of EE sources on the left
        for i in range(4,nr-4,2):
            self.lga.DrawLine(i,18,i+1,19,SRCV) # line of EE sources on the left
        #self.lga.DrawLine(mr-4,4,mr+4,4,SRC) # line of EE sources on the left
        #self.lga.DrawLine(mr-4,nc/4,mr+4,nc/4,SRCA) # line of omni-directional sources 1/4 from the left side
        #self.lga.DrawLine(mr-4,32,mr+4,32,SRCV) # line of WW sources in the middle
        self.lga.DrawLine(4,nc-4,nr-4,nc-4,SNK) # line of sinks on the right
        self.lga.DrawLine(mr-(nr/8)-(nr/8),(nc/8)+mc,mr+(nr/8)-(nr/8),(nc/8)+mc) # wall in the middle
        #self.lga.DrawLine(mr-(nr/4),mc,mr+(nr/4),mc) # wall in the middle
        #self.lga.DrawLine(mr,mc,mr-(nr/8),mc+(nc/8)) # top of wedge in the middle
        #self.lga.DrawLine(mr,mc,mr+(nr/8),mc+(nc/8)) # bottom of wedge in the middle
        return

    def setup_stats(self, mystatslist):
        # Define specific regions in which to compute statistics
        nr, nc = self.lga.A.shape
        statsrectL = (0, 0,    4+2+nr/2, nc/2)
        print "statsrectL = ", statsrectL
        mystatslist.append(statsrectL)

        statsrectR = (0, nc/2, 4+2+nr/2, nc)
        print "statsrectR = ", statsrectR
        mystatslist.append(statsrectR)
        return
