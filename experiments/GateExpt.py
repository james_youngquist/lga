from lga_experiment import Experiment
from lga_params import *

MAXR=64
MAXC=128
class GateExpt(Experiment):
    def __init__(self, *args):
        Experiment.__init__(self, *args)
        self.name = "Trying to build gates"

    def setup(self):
        self.currentStatFn = self.stat_density
        self.loadBlankLGA((MAXR,MAXC))
        self.setup_initialconditions()
        self.visualizer.lgaChanged()
        self.SRCVPARTICLES = WW

    def start(self):
        ENDTIME = 1000
        PRINTINTERVAL = 100
        # Define which function to use for the stats
        self.currentStatFn = self.stat_density
        self.loadBlankLGA((MAXR,MAXC))
        self.setup_particles()
        self.setup_boundaries()
        self.visualizer.lgaChanged()

        print "Running until ", ENDTIME
        while self.lga.t < ENDTIME:
            self.step()
            if (self.lga.t % PRINTINTERVAL == 0):
                print self.lga.t
        myax = self.visualizer.ui_plot_stats('Funnel demon test. ') # Show graph of stats

        return

    def setup_initialconditions(self):
        # Set up container / boundaries / world
        self.lga.DrawWalls() # This makes container
        
        self.lga.SRCVPARTICLES = WW
        nr, nc = self.lga.A.shape
        mr = nr/2 # middle row
        mc = nc/2 # middle col
        # A single one input on its own
        #self.lga.DrawPoint(25,10,EE) # Individual EE particle on the left

        # Two ones
        self.lga.DrawPoint(25,5,EE) # Individual EE particle on the left This needs to be delayed by 1

        #This delays it by one 
        self.lga.DrawPoint(25,10, EENE) # Glancing bounce up
        self.lga.DrawPoint(24,11, EEEE) # Reflect back down
        self.lga.DrawPoint(25,11, WWNW) # Get back on course

        self.lga.DrawPoint(27,5,EE) # Individual EE particle on the left

        self.lga.DrawPoint(27,30, EENE) # Specular wall This causes AND
        self.lga.DrawPoint(23,32, EENE) # Specular wall Steers +VE AND out from NE to EE

        # Delays the NO / garbage output
        self.lga.DrawPoint(25,55, EENE) # Glancing bounce up
        self.lga.DrawPoint(24,56, EEEE) # Reflect back down
        self.lga.DrawPoint(25,56, WWNW) # Get back on course
        #self.lga.DrawPoint(25,57, EENE) # Specular wall Delay by reflecting backward


        # Three particles
        self.lga.DrawPoint(49,5,EE) # Individual EE particle on the left This needs to be delayed by 1
        self.lga.DrawPoint(53,5,EE) # Individual EE particle on the left This needs to be delayed by 1
        self.lga.DrawPoint(57,5,EE) # Individual EE particle on the left This needs to be delayed by 1

        # Delays middle particle once
        self.lga.DrawPoint(53,35, EENE) # Glancing bounce up
        self.lga.DrawPoint(52,36, EEEE) # Reflect back down
        self.lga.DrawPoint(53,36, WWNW) # Get back on course

        # Delays middle particle a second time
        self.lga.DrawPoint(53,40, EENE) # Glancing bounce up
        self.lga.DrawPoint(52,41, EEEE) # Reflect back down
        self.lga.DrawPoint(53,41, WWNW) # Get back on course

        # Delays middle particle a third time
        self.lga.DrawPoint(53,44, EENE) # Glancing bounce up
        self.lga.DrawPoint(52,45, EEEE) # Reflect back down
        self.lga.DrawPoint(53,45, WWNW) # Get back on course


        return


    def setup_boundaries(self):
        # Set up container / boundaries / world
        self.lga.DrawWalls() # This makes container
        
        self.lga.SRCVPARTICLES = WW
        nr, nc = self.lga.A.shape
        mr = nr/2 # middle row
        mc = nc/2 # middle col
        self.lga.DrawLine(4,4,nr-4,4,SRC) # line of EE sources on the left
        for i in range(4,nr-4,2):
            self.lga.DrawLine(i,18,i+1,19,SRCV) # line of EE sources on the left
        #self.lga.DrawLine(mr-4,4,mr+4,4,SRC) # line of EE sources on the left
        #self.lga.DrawLine(mr-4,nc/4,mr+4,nc/4,SRCA) # line of omni-directional sources 1/4 from the left side
        #self.lga.DrawLine(mr-4,32,mr+4,32,SRCV) # line of WW sources in the middle
        self.lga.DrawLine(4,nc-4,nr-4,nc-4,SNK) # line of sinks on the right
        self.lga.DrawLine(mr-(nr/8)-(nr/8),(nc/8)+mc,mr+(nr/8)-(nr/8),(nc/8)+mc) # wall in the middle
        #self.lga.DrawLine(mr-(nr/4),mc,mr+(nr/4),mc) # wall in the middle
        #self.lga.DrawLine(mr,mc,mr-(nr/8),mc+(nc/8)) # top of wedge in the middle
        #self.lga.DrawLine(mr,mc,mr+(nr/8),mc+(nc/8)) # bottom of wedge in the middle
        return

    def setup_stats(self, mystatslist):
        # Define specific regions in which to compute statistics
        nr, nc = self.lga.A.shape
        statsrectL = (0, 0,    4+2+nr/2, nc/2)
        print "statsrectL = ", statsrectL
        mystatslist.append(statsrectL)

        statsrectR = (0, nc/2, 4+2+nr/2, nc)
        print "statsrectR = ", statsrectR
        mystatslist.append(statsrectR)
        return
