from lga_experiment import Experiment
from lga_params import *

MAXR=128
MAXC=128
class GateExpt2(Experiment):
    def __init__(self, *args):
        Experiment.__init__(self, *args)
        self.name = "Gates 2"

    def setup(self):
        self.currentStatFn = self.stat_density
        self.loadBlankLGA((MAXR,MAXC))
        self.setup_initialconditions()
        self.visualizer.lgaChanged()
        self.SRCVPARTICLES = WW

    def start(self):
        ENDTIME = 1000
        PRINTINTERVAL = 100
        # Define which function to use for the stats
        self.currentStatFn = self.stat_density
        self.loadBlankLGA((MAXR,MAXC))
        self.setup_particles()
        self.setup_boundaries()
        self.visualizer.lgaChanged()

        print "Running until ", ENDTIME
        while self.lga.t < ENDTIME:
            self.step()
            if (self.lga.t % PRINTINTERVAL == 0):
                print self.lga.t
        myax = self.visualizer.ui_plot_stats('Funnel demon test. ') # Show graph of stats

        return

    def setup_initialconditions(self):
        # Set up container / boundaries / world
        self.lga.DrawWalls() # This makes container
        
        self.lga.SRCVPARTICLES = WW
        nr, nc = self.lga.A.shape
        mr = nr/2 # middle row
        mc = nc/2 # middle col

        #self.lga.DrawPoint(11,84,SW) # 

        # Two particles
        self.lga.DrawPoint(69,5,EE) # Individual EE particle on the left
        self.lga.DrawPoint(73,5,EE) # Individual EE particle on the left


        # Delay top particle
        #self.lga.DrawDelay(69,25,12) # This is the older type of delay
        self.lga.DrawDelayTunnel(69,25,12)

        return


    def setup_boundaries(self):
        # Set up container / boundaries / world
        self.lga.DrawWalls() # This makes container
        
        self.lga.SRCVPARTICLES = WW
        nr, nc = self.lga.A.shape
        mr = nr/2 # middle row
        mc = nc/2 # middle col
        self.lga.DrawLine(4,4,nr-4,4,SRC) # line of EE sources on the left
        for i in range(4,nr-4,2):
            self.lga.DrawLine(i,18,i+1,19,SRCV) # line of EE sources on the left
        #self.lga.DrawLine(mr-4,4,mr+4,4,SRC) # line of EE sources on the left
        #self.lga.DrawLine(mr-4,nc/4,mr+4,nc/4,SRCA) # line of omni-directional sources 1/4 from the left side
        #self.lga.DrawLine(mr-4,32,mr+4,32,SRCV) # line of WW sources in the middle
        self.lga.DrawLine(4,nc-4,nr-4,nc-4,SNK) # line of sinks on the right
        self.lga.DrawLine(mr-(nr/8)-(nr/8),(nc/8)+mc,mr+(nr/8)-(nr/8),(nc/8)+mc) # wall in the middle
        #self.lga.DrawLine(mr-(nr/4),mc,mr+(nr/4),mc) # wall in the middle
        #self.lga.DrawLine(mr,mc,mr-(nr/8),mc+(nc/8)) # top of wedge in the middle
        #self.lga.DrawLine(mr,mc,mr+(nr/8),mc+(nc/8)) # bottom of wedge in the middle
        return

    def setup_stats(self, mystatslist):
        # Define specific regions in which to compute statistics
        nr, nc = self.lga.A.shape
        statsrectL = (0, 0,    4+2+nr/2, nc/2)
        print "statsrectL = ", statsrectL
        mystatslist.append(statsrectL)

        statsrectR = (0, nc/2, 4+2+nr/2, nc)
        print "statsrectR = ", statsrectR
        mystatslist.append(statsrectR)
        return
