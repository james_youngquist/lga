from lga_experiment import Experiment
from lga_params import *

class Experiment01(Experiment):
    #~~~~~ Boilerplate ~~~~~
    def __init__(self, *args):
        Experiment.__init__(self, *args)
        self.name = "Example Experiment"
        
    def setup(self):
        self.loadImageLGA("rgb-compose.png")
        print "Setup complete"
    #~~~~~ End Boilerplate ~~~~~

    def start(self):
        # Define which function to use for the stats
        self.currentStatFn = self.stat_density

        # Load the first LGA
        self.loadImageLGA("rgb-compose.png")
        while self.lga.t < 1000:
            self.step()
        self.visualizer.ui_plot_stats('Density') # Show graph of stats

        # Load the second LGA, which has no defined regions of interest
        self.loadImageLGA("particles.png") 
        while self.lga.t < 30:
            self.step()
        self.saveImg("step30.png") # save an image partway into the experiment
        while self.lga.t < 1000:
            self.step()
        self.visualizer.ui_plot_stats('Density') # Show (empty) graph of stats
        
        return

class Experiment02(Experiment):
    def __init__(self, *args):
        Experiment.__init__(self, *args)
        self.name = "A silly little thing"
        
    def setup(self):
        self.loadImageLGA("particles.png")

    def start(self):
        print "Not doing anything...still"

