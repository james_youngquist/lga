# Pressure-Volume Experiment
from lga_experiment import Experiment
from lga_params import *
import matplotlib; matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt
import math as m
import numpy as np

class ExptPressureVolSetup(Experiment):
    def __init__(self, *args):
        Experiment.__init__(self, *args)
        self.name = "Pressure Volume"

    global MAXR
    global MAXC
    global ENDTIME
    global PRINTINTERVAL
    MAXR = 128
    MAXC = 64
    ENDTIME = 1000
    PRINTINTERVAL = 100

    def reinit(self):
        self.currentStatFn = self.stat_pressure
        self.loadBlankLGA((MAXR,MAXC))
        self.setup_particles()
        self.setup_boundaries()
        self.lga.addRegion(250, 0, 0, MAXC-1, MAXR-1) # Full piston
        self.visualizer.lgaChanged()
        return

    def setup(self): # gets called when program starts, or when reset occurs
        self.reinit()
        self.draw_piston_f(0.2) # Nice place to put it just so you can see it...make sure particle cloud not too big
        self.visualizer.lgaChanged()


    def rununtil(self,nsteps, theax,thelabel): # run for some number of steps
        print "Running until ", ENDTIME
        while self.lga.t < ENDTIME:
            self.step()
            if (self.lga.t % PRINTINTERVAL == 0):
                print self.lga.t
        for reg_id, stats in self.lga.stattimeseries.items():
            theax.plot(self.lga.timelist, stats, label=thelabel)
            nps =np.array(stats)
            mn = nps.mean()
            print "label =", thelabel, "  mean of stats = ", mn
        handles, labels = theax.get_legend_handles_labels()
        theax.legend(handles, labels)
        return mn # return the last computed mean

    def start(self): # gets called when start gets clicked
        myax = self.visualizer.ui_plot_stats('Pressure vs Vol') # Show graph of stats
        pressurelist = []
        #pistonposlist =np.array([0.2,0.3,0.4,0.6,0.8])
        pistonposlist =np.array([0.2, 0.4, 0.8])
        vollist = pistonposlist*MAXC*MAXR
        for pistonf in pistonposlist:
            self.reinit()
            print "pistonf = ",pistonf
            self.draw_piston_f(pistonf)
            self.visualizer.lgaChanged()
            self.saveImg("PistonAt"+str(pistonf)+"Init.png")
            meanpressure=self.rununtil(ENDTIME,myax,"Piston at "+str(pistonf))
            print "meanpressure= ", meanpressure
            pressurelist.append(meanpressure)
            self.saveImg("PistonAt"+str(pistonf)+"Eq.png")

            self.reinit()
            self.draw_piston_f(pistonf)
            nbd = self.stat_measureboundary()
            print "Testing new measure function.", nbd
        myax.redraw_in_frame()

        perimlist = 2*MAXC+2*(pistonposlist*MAXR)
        pressurelist = pressurelist / perimlist

        plt.figure()
        plt.plot(vollist,pressurelist)
        plt.title("P vs V")
        plt.xlabel("V")
        plt.ylabel("P")
        plt.show()

        plt.figure()
        pvlist = pressurelist*vollist
        plt.plot(vollist,pvlist)
        plt.title("P * V vs V")
        plt.xlabel("V")
        plt.ylabel("V * P")
        plt.ylim(0.0,1.0+pvlist.max())
        plt.show()

        return


    def setup_particles(self):
        # Put in a bunch of random particles in a rectangular shape
        nr, nc = self.lga.A.shape
        print "Setting up particles L! nr=",nr, " nc=",nc
        rectLsml = (4,4,-4+nr/5,-4+nc) # make sure particles don't overlap walls
        print "rectLsml = ", rectLsml
        self.lga.RectNFill(0.5,rectLsml) #Fill Left side with exactly p*vol(rect) particles
        return

    # Draw piston at a specified _fraction_ of the way
    def draw_piston_f(self, frac):
        nr, nc = self.lga.A.shape
        posn = int(frac*nr)
        self.lga.DrawLine(posn,0,posn,nc) # Piston
        return

    # Draw piston at a specified _row_
    def draw_piston_r(self, row):
        nr, nc = self.lga.A.shape
        self.lga.DrawLine(row,0,row,nc) # Piston
        return

    def setup_boundaries(self):
        # Set up container / boundaries / world
        self.lga.DrawDoubleWalls() # This makes double-walled container        
        return

    # def setup_stats(self, mystatslist):
    #     # Define specific regions in which to compute statistics
    #     nr, nc = self.lga.A.shape
    #     statsrectL = (0, 0,    4+2+nr/2, nc/2)
    #     print "statsrectL = ", statsrectL
    #     mystatslist.append(statsrectL)

    #     statsrectR = (0, nc/2, 4+2+nr/2, nc)
    #     print "statsrectR = ", statsrectR
    #     mystatslist.append(statsrectR)
    #     return
