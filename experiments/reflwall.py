from lga_experiment import Experiment
from lga_params import *

class ReflTest(Experiment):
    def __init__(self, *args):
        Experiment.__init__(self, *args)
        self.name = "Test Reflecting Walls"
        
    def setup(self):
        self.currentStatFn = self.stat_density
        self.loadBlankLGA((128,128))
        self.setup_particles()
        self.setup_boundaries()
        self.lga.addRegion(250, 0, 0, 63, 127)
        self.lga.addRegion(240, 64, 0, 127, 127)
        self.visualizer.lgaChanged()

    def start(self):
        ENDTIME = 1000
        PRINTINTERVAL = 100
        # Define which function to use for the stats
        self.currentStatFn = self.stat_density
        self.loadBlankLGA((128,64))
        self.setup_particlesL()
        self.setup_boundaries()
        self.lga.addRegion(240, 32, 0, 63, 127)
        self.lga.addRegion(250, 0, 0, 31, 127)
        self.visualizer.lgaChanged()

        print "Running until ", ENDTIME
        while self.lga.t < ENDTIME:
            self.step()
            if (self.lga.t % PRINTINTERVAL == 0):
                print self.lga.t
        myax = self.visualizer.ui_plot_stats('Funnel demon test. ') # Show graph of stats

        # Load the second LGA
        self.loadBlankLGA((128,64))
        self.setup_particlesR()
        self.setup_boundaries()
        self.lga.addRegion(240, 32, 0, 63, 127)
        self.lga.addRegion(250, 0, 0, 31, 127)
        self.visualizer.lgaChanged()
        print "Running until ", ENDTIME
        while self.lga.t < ENDTIME:
            self.step()
            if (self.lga.t % PRINTINTERVAL == 0):
                print self.lga.t
        #self.visualizer.ui_plot_stats('Density (plot 2)') # Show (empty) graph of stats
        print "Plotting exp 2"
        for reg_id, stats in self.lga.stattimeseries.items():
            myax.plot(self.lga.timelist, stats, label='Region ' + str(reg_id)+ ' R to L')
            print "reg_id =", reg_id
        handles, labels = myax.get_legend_handles_labels()
        myax.legend(handles, labels)
        myax.redraw_in_frame()
        # need a show here
        #plt.show()
        #self.visualizer.lgaChanged()
        return


    def setup_particles(self):
        self.setup_particlesL()
        return

    def setup_particlesL(self):
        # Put in a bunch of random particles in a rectangular shape
        nr, nc = self.lga.A.shape
        print "Setting up particles L! nr=",nr, " nc=",nc
        rectLsml = (20,4,-4+2+nr/2,-4+nc/2) # shrink the square so that the particles don't overlap walls
        print "rectLsml = ", rectLsml
        self.lga.RectNFill(0.5,rectLsml) #Fill Left side with exactly p*vol(rect) particles
        return

    def setup_particlesR(self):
        # Put in a bunch of random particles in a rectangular shape
        nr, nc = self.lga.A.shape
        rectRsml = (20,4+nc/2,-4+2+nr/2,-4+nc) # this ensures that all particles we put in initially remain (don't get overwritten by walls)
        print "rectRsml = ", rectRsml
        self.lga.RectNFill(0.5,rectRsml) #Fill Left side with exactly p*vol(rect) particles
        return

    def setup_particlesB(self):
        # Put in a bunch of random particles in a rectangular shape
        nr, nc = self.lga.A.shape
        rectLsml = (30,4,-4+2+nr/2,-4+nc/2) # shrink the square so that the particles don't overlap walls
        print "rectLsml = ", rectLsml
        self.lga.RectNFill(0.5,rectLsml) #Fill Left side with exactly p*vol(rect) particles

        rectRsml = (30,4+nc/2,-4+2+nr/2,-4+nc) # this ensures that all particles we put in initially remain (don't get overwritten by walls)
        print "rectRsml = ", rectRsml
        self.lga.RectNFill(0.5,rectRsml) #Fill Left side with exactly p*vol(rect) particles
        return

    def setup_boundaries(self):
        # Set up container / boundaries / world
        self.lga.DrawWalls() # This makes container
        
        nr, nc = self.lga.A.shape
        mr = nr/2 # middle row
        mc = nc/2 # middle col
        self.lga.DrawLine(0,   mc-1,   mr+1, mc-1) # vertical wall to left of middle of upper half
        self.lga.DrawLine(0,   mc, mr+1, mc) # vertical wall to right of middle of upper half

        self.lga.DrawLine(2,0,2,nc) # horz wall---very top of container
        self.lga.DrawLine(2+mr,0,2+mr,nc) # Bottom of container, offset by 2

        # Symmetrical rectifier design
        self.lga.DrawLine(mr,  0.25*nc,   mr,   0.75*nc) # "rectifier": horz wall along middle bottom
        self.lga.DrawLine(mr-2,  2+0.75*nc, 2+mr,   2+0.75*nc) # "rectifier": small vert wall to make it harder for particles to enter
        self.lga.DrawLine(2,nc/4,6,nc/4) # small vert wall in left chamber to make LR volume same
        return

    def setup_stats(self, mystatslist):
        # Define specific regions in which to compute statistics
        nr, nc = self.lga.A.shape
        statsrectL = (0, 0,    4+2+nr/2, nc/2)
        print "statsrectL = ", statsrectL
        mystatslist.append(statsrectL)

        statsrectR = (0, nc/2, 4+2+nr/2, nc)
        print "statsrectR = ", statsrectR
        mystatslist.append(statsrectR)
        return
