import codecs
import numpy as np
from lga_params import *

wallRotMap = {
    0: NENW,
    30: NWNW,
    60: WWNW,
    90: WWWW,
    120: WWSW,
    150: SWSW,
    180: SWSE,
    210: SESE,
    240: EESE,
    270: EEEE,
    300: EENE,
    330: NENE
    }

particleMap = {
    30: NW,
    90: WW,
    150: SW,
    210: SE,
    270: EE,
    330: NE
    }

def parseFields(line):
    cells = line.split('\t')
    category = cells[0]
    pairs = [c.split(':') for c in cells[1:]]
    mapping = dict(pairs)
    mapping['cat'] = category
    return mapping


def parseHexFile(filename):
    hexfile = codecs.open(filename, encoding='utf-16')
    lines = hexfile.readlines()
    info = lines[0].split(',')
    width, height = map(int, info[:2])
    A = np.zeros([height, width], dtype = np.int)

    idx = 0
    while 'hexes' not in lines[idx][:10]:
        idx += 1
    idx += 1

    row = -1
    col = 0

    while idx != len(lines):
        fields = parseFields(lines[idx])
        idx += 1
        
        if fields['cat'] == 'Blank':
            row += 1
            if row == height:
                row = 0
                col += 1
                
        elif fields['cat'] == 'feature':
            r = int(float(fields['rotation']))
            if fields['type'] == 'Railroad N-S':
                # Directional wall
                A[row,col] += wallRotMap[r]
                
            elif fields['type'] == 'Railroad N-S-NE-NW-SE-SW':
                # Reversing wall
                A[row,col] += REV
                
            elif fields['type'] == 'Oil Well':
                # Particles
                if r in [30, 90, 150, 210, 270, 330]:
                    A[row,col] += particleMap[r]
                else:
                    print '(%d, %d): %d is not a valid particle rotation'%(row, col, r)
                    
            else:
                # Haven't assigned a purpose for this feature yet
                print '(%d, %d): Unused feature "%s".'%(row, col, fields['type'])
                
        else:
            print 'Unused category "%s" on line %d'%(fields['type'], idx)

    return A
