# lga_params.py: constants used throughout

#global nr, nc, MAXT, MD, NBITS
#cdef int  nr, nc, MAXT, MD, NBITS

MD = 3 # MaxDigits (how many spaces to hold when printing cells)

#   NW bit 2       NE bit 1
#            \    /
#             \  /
#  WW bit 3----  ---- EE bit 0
#             /  \
#            /    \
#    SW bit 4      SE bit 5
# bit 6:    stationary particle 
# bit 7:    collision chirality 
# bit 8-10: boundary type


#global    EE,NE,NW,WW,SW,SE,ZZ,CC,BD0,BD1,BD2,BD3,REV,EEEE,EENE,NENE,NENW,NWNW,WWNW
#cdef int  EE,NE,NW,WW,SW,SE,ZZ,CC,BD0,BD1,BD2,BD3,REV,EEEE,EENE,NENE,NENW,NWNW,WWNW
EE = 2**0
NE = 2**1
NW = 2**2
WW = 2**3
SW = 2**4
SE = 2**5
ZZ = 2**6   # Stationary particle
CC = 2**7   # Collision Chirality
BD0 = 2**8  # Boundary bit 0
BD1 = 2**9  # Boundary bit 1
BD2 = 2**10 # Boundary bit 2
BD3 = 2**11 # Boundary bit 3
NBITS = 12  # Should always be one greater than exponent from row above


"""
   Boundary Conditions (walls):
     6 x specular reflection (need to specify wall direction: 6 possibilities for FHP, assuming symmetrical, and full & half angles)
        0000 ==  0 no boundary
        0001 ==  1 EE-EE boundary
        0010 ==  2 EE-NE boundary
        0011 ==  3 NE-NE boundary
        0100 ==  4 NE-NW boundary
        0101 ==  5 NW-NW boundary
        0110 ==  6 WW-NW boundary
        0111 ==  7 REV = BD0+BD1+BD2:  reversing boundary
        1000 ==  8 SRC    # SRC: puts out EE-going particles
        1001 ==  9 SNK    # SNK: absorbs particles from all directions
        1010 == 10 HOT
        1011 == 11 CLD
        1100 == 12 SRCA  # SRC that emits particles in all directions
        1101 == 13 SRCV  # SRC that emits particles according to global variable SRCVPARTICLES
        1110 == 14 
        1111 == 15
        ^  ^
        |  |
      BD3  BD0 
"""
EEEE = BD0
EENE = BD1
NENE = BD0+BD1
NENW = BD2
NWNW = BD2+BD0
WWNW = BD2+BD1
REV  = BD2 + BD1 + BD0 # defines 'opcode' REV to be 0111 of the boundary bits
SRC  = BD3
SNK  = BD3 + BD0
HOT  = BD3 + BD1
CLD  = BD3 + BD1 + BD0
SRCA = BD3 + BD2
SRCV = BD3 + BD2 + BD0


# The angles from WWWW through EESE are actually just aliases
# for the angles from EEEE through WWNW ... they are redundant
WWWW = EEEE
WWSW = EENE
SWSW = NENE
SWSE = NENW
SESE = NWNW
EESE = WWNW


# dictionaries for converting between particle bit codes and discrete wall angles
PARTBIT2WALLANG = {EE:0, NE:2, NW:4, WW:6, SW:8, SE:10} 
WALLANG2PARTBIT = {0:EE, 2:NE, 4:NW, 6:WW, 8:SW, 10:SE}

MAXANG = 12 # Use this as modulus in angle calcs
MAXHALFANG = 6
# dictionaries for converting between wall bit codes and discrete angles
# if you take a redundant angle (e.g. WWWW) mod MAXANG, you get the right thing (e.g. EEEE)
WALLANG2BIT= {0:EEEE, 1:EENE, 2:NENE, 3:NENW,  4:NWNW,  5:WWNW, 
              6:WWWW, 7:WWSW, 8:SWSW, 9:SWSE, 10:SESE, 11:EESE}
WALLBIT2ANG= {EEEE:0, EENE:1, NENE:2, NENW:3, NWNW:4,  WWNW:5, 
              WWWW:6, WWSW:7, SWSW:8, SWSE:9, SESE:10, EESE:11}

#global MOVINGMASK   # Mask for detecting moving particles
#cdef int  MOVINGMASK   # Mask for detecting moving particles
MOVINGMASK = EE+NE+NW+WW+SW+SE

#global PARTMASK     # Mask for detecting any particle (including stationary)
#cdef int  PARTMASK     # Mask for detecting any particle (including stationary)
PARTMASK   = EE+NE+NW+WW+SW+SE+ZZ 

#global BDMASK     # Mask for detecting boundary
#cdef int  BDMASK     # Mask for detecting boundary
BDMASK   = BD0+BD1+BD2+BD3

# These constants identify where the neighbors are
# They encode the structure of the hexagonal lattice
# For example EE_E_j tells for an _E_ven cell (i.e. one that is in an even numbered row),
# the column coordinate (j) of the East (EE) neighbor
# Note that the top row is numbered 0 and therefore is even.
#global EE_E_j,NE_E_j,NW_E_j,WW_E_j,SW_E_j,SE_E_j    
#cdef int  EE_E_j,NE_E_j,NW_E_j,WW_E_j,SW_E_j,SE_E_j    
EE_E_j = - 1 # East-East _ Dest is Even _ j coord    DST H, SRC G
NE_E_j = - 1 # NorthEast _ Dest is Even _ j coord    DST H, SRC J
NW_E_j = + 0 # NorthWest _ Dest is Even _ j coord    DST H, SRC K
WW_E_j = + 1 # West-West _ Dest is Even _ j coord    DST H, SRC I
SW_E_j = + 0 # SouthWest _ Dest is Even _ j coord    DST H, SRC E
SE_E_j = - 1 # SouthEast _ Dest is Even _ j coord    DST H, SRC D

#global EE_E_i,NE_E_i,NW_E_i,WW_E_i,SW_E_i,SE_E_i 
#cdef int  EE_E_i,NE_E_i,NW_E_i,WW_E_i,SW_E_i,SE_E_i 
#i coord (vertical)
EE_E_i = + 0 # East-East _ Dest is Even _ i coord    DST H, SRC G
NE_E_i = + 1 # NorthEast _ Dest is Even _ i coord    DST H, SRC J
NW_E_i = + 1 # NorthWest _ Dest is Even _ i coord    DST H, SRC K
WW_E_i = + 0 # West-West _ Dest is Even _ i coord    DST H, SRC I
SW_E_i = - 1 # SouthWest _ Dest is Even _ i coord    DST H, SRC E
SE_E_i = - 1 # SouthEast _ Dest is Even _ i coord    DST H, SRC D

#global EE_O_j,NE_O_j,NW_O_j,WW_O_j,SW_O_j,SE_O_j
#cdef int  EE_O_j,NE_O_j,NW_O_j,WW_O_j,SW_O_j,SE_O_j
EE_O_j = - 1 # East-East _ Dest is Odd _ j coord    DST E, SRC D
NE_O_j = + 0 # NorthEast _ Dest is Odd _ j coord    DST E, SRC H
NW_O_j = + 1 # NorthWest _ Dest is Odd _ j coord    DST E, SRC I
WW_O_j = + 1 # West-West _ Dest is Odd _ j coord    DST E, SRC F
SW_O_j = + 1 # SouthWest _ Dest is Odd _ j coord    DST E, SRC C
SE_O_j = + 0 # SouthEast _ Dest is Odd _ j coord    DST E, SRC B

#global EE_O_i,NE_O_i,NW_O_i,WW_O_i,SW_O_i,SE_O_i
#cdef int  EE_O_i,NE_O_i,NW_O_i,WW_O_i,SW_O_i,SE_O_i
# i coord (vertical)
EE_O_i = + 0 # East-East _ Dest is Odd _ i coord    DST E, SRC D
NE_O_i = + 1 # NorthEast _ Dest is Odd _ i coord    DST E, SRC H
NW_O_i = + 1 # NorthWest _ Dest is Odd _ i coord    DST E, SRC I
WW_O_i = + 0 # West-West _ Dest is Odd _ i coord    DST E, SRC F
SW_O_i = - 1 # SouthWest _ Dest is Odd _ i coord    DST E, SRC C
SE_O_i = - 1 # SouthEast _ Dest is Odd _ i coord    DST E, SRC B

#global Dirs_E_i,Dirs_E_j,Dirs_O_i,Dirs_O_j
#global Dirs_E_i,Dirs_E_j,Dirs_O_i,Dirs_O_j
Dirs_E_i = [EE_E_i,NE_E_i,NW_E_i,WW_E_i,SW_E_i,SE_E_i]
Dirs_E_j = [EE_E_j,NE_E_j,NW_E_j,WW_E_j,SW_E_j,SE_E_j]
Dirs_O_i = [EE_O_i,NE_O_i,NW_O_i,WW_O_i,SW_O_i,SE_O_i]
Dirs_O_j = [EE_O_j,NE_O_j,NW_O_j,WW_O_j,SW_O_j,SE_O_j]        
