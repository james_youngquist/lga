# lga_ui.py: ui front end to lga simulation
import math as m
import numpy as np
from random import randint
import matplotlib; matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt
import sys, os, random
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from matplotlib.widgets import Button
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from scipy.misc import imresize
import time
import matplotlib.animation as animation
from matplotlib import colors
import threading

from lga_params import *
import lga_experiment
from lga_sim import LGA

class AppForm(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self,parent)
        self.setWindowTitle('lga')
        self.button_col = 0
        self.button_row = 0
        self.button_layout = QGridLayout()
        self.experiments = {}
        self.findExperiments()
        self.create_main_frame()

    def onestat(self):
        self.anLGA.onestat()
        return

    def print_state(self):
        self.anLGA.print_state()
        return

    def lgaChanged(self):
        self.anLGA = self.experiment.lga
        
        # Refresh display
        self.InitDisplay()
        
    def InitDisplay(self):
        fcs = (1.0/7.0)/4.0 # float color step
        cs = 255/7;
        mycolmap = np.zeros((22,3),dtype=float) #here
        for c in range(1,8):
            mycolmap[c,0] = 0.75+c*fcs                    
            mycolmap[c,1] = 0.75+c*fcs                    
            mycolmap[c,2] = 0.75+c*fcs                
        for c in range(8,22):
            mycolmap[c,0] = (1.0/22.0)*c                    
            mycolmap[c,1] = 0.0                    
            mycolmap[c,2] = 0.0                    
        #cmap = colors.ListedColormap(['black', 'red', 'orange', 'yellow', 'green', 'blue', 'purple', 'brown', 'white'])
        cmap = colors.ListedColormap(mycolmap)
        self.cmap = cmap

        #print "mycolmap: "
        #print mycolmap
        #print
        bounds = [0,1,2,3,4,5,6,7,8,15,20,40,60,80,100,120,140,160,180,200,220,240,255]
        if ((len(bounds)-1) != cmap.N):
            print "Warning: color interpolation will be used"
        #print "len(bounds)= ", len(bounds), "  Bounds for colors: "
        #print bounds
        #print
        #print "cmap.N"
        #print cmap.N
        norm = colors.BoundaryNorm(bounds, cmap.N+1)
        self.norm = norm

        self.anLGA.computeImg(self.anLGA.A)
        self.im=self.axes.imshow(self.anLGA.img, cmap=cmap, norm=norm,interpolation='nearest')
        self.canvas.draw()
        return

    #########################################################################    
    ### To be edited for each experiment
    #exp
    #
    # This defines the logic that controls the experiment
    # For a batch-mode system, you wouldn't need this function
    # This implements the experiment logic onto the event-driven
    # structure provided by the GUI
    #########################################################################    
    #
    # TODO: think about how/when to reset the expt_state
    def expt_control(self):
        self.shouldrun = self.run_box.isChecked()
        
        if self.shouldrun:
            self.anLGA.step()
            # print "Update time:", self.anLGA.update_time
            self.ui_redraw()
        return

    # This does not work for some reason
    def ui_startstop(self):
        print "startstop"
        if self.run_box.isChecked():
            print "Is Checked"
        else:
            print "Is NOT Checked"
        return

    def ui_showflow(self):
        self.showflow = self.showflow_box.isChecked()
        self.ui_redraw()
        return

    def ui_edited_flow_subsample(self):
        self.flow_subsample = int(self.flow_subsample_textedit.text())
        return

    def ui_edited_framerate(self):
        self.interval = self.framerate_text.value()
        self.timer.start(self.interval)

    def ui_edited_statstep(self):
        self.experiment.steps_per_stat = self.statstep_text.value()

    def ui_execute_script(self):
        filename = str(self.script_text.text())
        if len(filename) > 0:
            try:
                with open(filename): pass
                execfile(filename)
            except IOError:
                QMessageBox.about(self, "Oops", "File doesn't exist: %s"%(filename))
        else:
            QMessageBox.about(self, "Oops", "Please choose a file")
            
        return

    def ui_choose_script(self):
        self.script = QFileDialog.getOpenFileName(self, "Choose script", ".", "Python (*.py)")
        self.script_text.setText(self.script)

    def ui_load_image(self):
        filename = str(self.image_text.text())
        if len(filename) > 0:
            try:
                with open(filename): pass
                self.experiment.loadImageLGA(filename)
            except IOError:
                QMessageBox.about(self, "Oops", "File doesn't exist: %s"%(filename))
        else:
            QMessageBox.about(self, "Oops", "Please choose a file")
            
        return

    def ui_choose_image(self):
        self.image = QFileDialog.getOpenFileName(self, "Choose image", ".", "PNG (*.png)")
        self.image_text.setText(self.image)

        
    def ui_redraw(self):
        elapsed = time.time() - self.last_redraw_time
        if elapsed > 1/30.0:
            self.time_label.setText("Time: "+str(self.anLGA.t) + "  dt: "+str(self.anLGA.dt)+ "  expt state: "+str(self.expt_state))
            self.anLGA.computeImg(self.anLGA.A)
            lga = self.experiment.lga

            ax = self.axes
            ax.cla()
            if self.showflow:
                A = lga.A
                nr,nc = A.shape
                samples = self.flow_subsample
                pi = np.pi
                fnr = nr/samples
                fnc = nc/samples
                X = samples*np.arange(0,fnc,1) + samples/2.0
                Y = samples*np.arange(0,fnr,1) + samples/2.0
                U = np.zeros((fnr,fnc), dtype=np.double)
                V = np.zeros((fnr,fnc), dtype=np.double)
                F = np.zeros((fnr,fnc,3), dtype=np.double)
                vecs = [np.array([np.sin(a), np.cos(a)]) for a in [0, pi/3, 2*pi/3, pi, 4*pi/3, 5*pi/3]]
                bits = [2**b for b in xrange(6)]
                deltas = tuple(range(samples))

                # change this to be correct
                max_len = 0.0
                for y in xrange(fnr):
                    for x in xrange(fnc):
                        for v in deltas:
                            for u in deltas:
                                for b in xrange(6):
                                    if A[y*samples+v, x*samples+u] & bits[b]:
                                        V[y,x] += vecs[b][0]
                                        U[y,x] += vecs[b][1]
                        angle = np.arctan2(V[y,x],U[y,x]) + np.pi
                        length = np.sqrt(V[y,x]**2 + U[y,x]**2)

                        # Convert from HSV to RGB
                        Hp = angle / (np.pi/3)
                        C = length
                        W = C * (1 - np.abs((Hp % 2) - 1))
                        rgb = (0.0, 0.0, 0.0)
                        if Hp < 1:
                            rgb = (C, W, 0.0)
                        elif Hp < 2:
                            rgb = (W, C, 0.0)
                        elif Hp < 3:
                            rgb = (0.0, C, W)
                        elif Hp < 4:
                            rgb = (0.0, W, C)
                        elif Hp < 5:
                            rgb = (W, 0.0, C)
                        elif Hp < 6:
                            rgb = (C, 0.0, W)
                        F[y,x] = rgb

                        # Track max length for renormalization
                        if length > max_len:
                            max_len = length

                # renormalize
                F[y,x,:] /= max_len

                fimg = imresize(F, A.shape, 'bicubic')
                ax.imshow(fimg)
                Q = ax.quiver(X,Y,U,V, pivot='middle', color=(1,1,1,.4))
                ax.imshow(lga.img, interpolation='nearest', alpha=0.2, cmap=form.cmap, norm=form.norm)
            else:
                ax.imshow(lga.img, interpolation='nearest', cmap=form.cmap, norm=form.norm)
                
            #plt.show()

            #self.im.set_data(self.anLGA.img)
            self.canvas.draw()
            self.last_redraw_time = time.time()
        return
    
    def ui_onestep(self):
        self.anLGA.step()
        self.ui_redraw()
        return

    def ui_reverse(self):
        self.anLGA.reverse()
        self.ui_redraw()
        return

    def ui_start(self):
        self.experiment.start()
        self.ui_redraw()

    def ui_reset(self):
        self.ui_chose_experiment(self.expQList.currentItem())
        # self.anLGA.setup()
        # self.InitDisplay()
        # # Experiment control logic
        # self.flow_subsample = int(self.flow_subsample_textedit.text())
        # self.expt_state = 0 # Variable that the UI uses to keep track of the state of the experiment
        # self.shouldrun = False
        # self.ui_redraw()
        return

    def ui_reset_expt_state(self):
        self.flow_subsample = int(self.flow_subsample_textedit.text())
        self.expt_state = 0 # Variable that the UI uses to keep track of the state of the experiment
        self.shouldrun = False
        self.ui_redraw()
        return

    # This gets called when you click the PlotStats button
    def ui_plot_stats(self, name=''):
        #def plotit(gui):
        lga = self.anLGA
        lga.computeImg(lga.A)   # update image
        plt.figure()

        # Plot statistics
        ax = plt.subplot(1,2,1)
        plt.title('%s %s at time %d'%(name, lga.plotlabel, lga.t))
        for reg_id, stats in lga.stattimeseries.items():
            ax.plot(lga.timelist, stats, label='Region ' + str(reg_id))
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles, labels)

        # Plot current state
        ax = plt.subplot(1,2,2)
        ax.imshow(lga.img, cmap=self.cmap, norm=self.norm,interpolation='nearest')
        ax.imshow(lga.regions_img, interpolation='nearest', cmap=matplotlib.cm.copper, alpha=0.3)
        for reg_id, region in lga.regions.items():
            # need to swap x,y coordinates because they are stored as y,x
            ax.annotate('%s'%reg_id, (region[0,1],region[0,0]), color=(0,1,0))

        plt.tight_layout()
        plt.show()
        #plotit(self)
        #thread.start_new_thread(plotit, (self,))
        return plt.subplot(1,2,1)

    def ui_chose_experiment(self, item):
        newExp = self.experiments[str(item.text())]
        self.experiment = newExp(self)
        self.experiment.setup()
        self.statstep_text.setValue(self.experiment.steps_per_stat)
        self.experiment_label.setText("%s (%s)"%(self.experiment.name,
                                                 self.experiment.__class__))
        
        
    def refresh_expQList(self):
        '''
        Updates the list of Experiments in the UI.
        '''
        self.expQList.clear()
        for name, pytype in self.experiments.items():
            QListWidgetItem(name, self.expQList)
        self.expQList.setCurrentRow(0)

    def ui_refresh_expQList(self):
        '''
        Call this to rescan the disk for any changes
        '''
        self.findExperiments()
        self.refresh_expQList()

    def addButton(self, name, text, callback):
        '''
        name - name for the field, so you can do self.name
        text - text to appear on the button in the GUI
        callback - function to call when the button is pressed
        '''
        widget = None
        if hasattr(self, name):
            print 'WARNING: A button with the name "%s" already exists.'%(name)
            print '         Reassigning its text and callback function'
            widget = getattr(self, name)
            widget.clicked.disconnect()
            widget.setText(text)
        else:
            widget = QPushButton(text, self)
            setattr(self, name, widget)
            self.button_layout.addWidget(widget, self.button_row, self.button_col)
            self.button_col += 1
            if self.button_col == 3:
                self.button_row += 1
                self.button_col = 0
        widget.clicked.connect(callback)

    def findExperiments(self):
        import pkgutil
        import experiments
        reload(experiments)
        self.experiments = {}
        prefix = experiments.__name__ + '.'
        for importer, modname, ispkg in pkgutil.iter_modules(experiments.__path__, prefix):
            if not ispkg:
                module = __import__(modname, fromlist='dummy')
                reload(module)
            for name in dir(module):
                obj = getattr(module, name)
                if type(obj) is type and lga_experiment.Experiment in obj.__bases__:
                    print name
                    # We have found an experiment!
                    self.experiments[obj.__name__] = obj
        return
    
    def create_main_frame(self):
        # Experiment control logic...need to allocate these variables
        self.flow_subsample = 4 # this value will be replaced by one from UI once UI is set up
        self.showflow = False
        self.expt_state = 0 # Variable that the UI uses to keep track of the state of the experiment
        self.shouldrun = False
        self.last_redraw_time = time.time()
        
        self.main_frame = QWidget()

        self.experiment_label = QLabel()

        self.fig = Figure((5.0, 5.0), dpi=100) 
        #self.fig = Figure((5.0, 4.0)) 
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self.main_frame)
        self.axes = self.fig.add_subplot(111)
        #H = np.zeros((nr,nc),dtype=int)

        # Run Enable Box
        self.run_box = QCheckBox("Run",self)
        self.connect(self.run_box, SIGNAL('stateChanged()'), self.ui_startstop)

        # Vector flow display Box
        self.showflow_box = QCheckBox("Show Flow",self)
        self.showflow_box.stateChanged.connect(self.ui_showflow)
        
        # Time label
        self.time_label = QLabel(self)

        # Sub samples
        self.flow_subsample_textedit = QLineEdit("4",self)
        self.flow_subsample_textedit.textChanged.connect(self.ui_edited_flow_subsample)

        # Framerate limit label
        self.framerate_label = QLabel(self)
        self.framerate_label.setText('Frame period limit (ms)')

        # Framerate limit textbox
        self.framerate_text = QSpinBox(self)
        self.framerate_text.setValue(33)
        self.framerate_text.setMaximum(100000)
        self.connect(self.framerate_text, SIGNAL('valueChanged(int)'), self.ui_edited_framerate)

        # Statstep label
        self.statstep_label = QLabel(self)
        self.statstep_label.setText('Stat compute interval')

        # Statstep textbox
        self.statstep_text = QSpinBox(self)
        self.statstep_text.setValue(10)
        self.statstep_text.setMaximum(100000)
        self.connect(self.statstep_text, SIGNAL('valueChanged(int)'), self.ui_edited_statstep)

        # Script execute button
        self.script_button = QPushButton('Execute script', self)
        self.connect(self.script_button, SIGNAL('clicked()'), self.ui_execute_script)

        # Script textbox
        self.script_text = QLineEdit('', self)

        # Script choose button
        self.script = ''
        self.script_choose = QPushButton('...', self)
        self.connect(self.script_choose, SIGNAL('clicked()'), self.ui_choose_script)

        # Image execute button
        self.image_button = QPushButton('Load image', self)
        self.connect(self.image_button, SIGNAL('clicked()'), self.ui_load_image)

        # Image textbox
        self.image_text = QLineEdit('', self)

        # Image choose button
        self.image = ''
        self.image_choose = QPushButton('...', self)
        self.connect(self.image_choose, SIGNAL('clicked()'), self.ui_choose_image)

        # Plot Stats
        self.addButton('plot_stats_button', 'Plot Stats', self.ui_plot_stats)

        # Single step Button
        self.addButton('step_button', 'One Step', self.ui_onestep)

        # Print One Stat --- print statistics for most recent step
        self.addButton('onestat_button', 'Print Current Stats', lambda a: self.onestat)

        # Reverse Button
        self.addButton('reverse_button', 'Reverse', self.ui_reverse)

        # Print State
        self.addButton('print_button', 'Print', self.print_state)

        # Start experiment
        self.addButton("start_button", "Start Experiment", self.ui_start)

        # Reset Button
        self.addButton('reset_button', 'Reset All', self.ui_reset)

        # "Reset Expt State" Button
        #self.addButton('reset_expt_state_button', 'Reset Expt State', self.ui_reset_expt_state)

        # Quit button
        self.addButton('quit_button', 'Quit', sys.exit)

        # Set up Experiment list widgets
        self.expQList = QListWidget(self)
        self.expQList.itemDoubleClicked.connect(self.ui_chose_experiment)
        self.refresh_expQList()
        self.expQList.setCurrentRow(0)
        self.refresh_expQList_button = QPushButton("Refresh", self)
        self.refresh_expQList_button.clicked.connect(self.ui_refresh_expQList)
        
        #Timer
        self.interval = self.framerate_text.value()
        self.timer = QTimer()
        self.timer.timeout.connect(self.expt_control)
        self.timer.start(self.interval)

        hbox = QHBoxLayout()
        hbox.addWidget(self.run_box)
        hbox.addWidget(self.showflow_box)
        hbox.addWidget(self.time_label)
        hbox.addStretch(1)
        hbox.addWidget(self.flow_subsample_textedit)

        framerate_hbox = QHBoxLayout()
        framerate_hbox.addWidget(self.framerate_label)
        framerate_hbox.addWidget(self.framerate_text)

        statstep_hbox = QHBoxLayout()
        statstep_hbox.addWidget(self.statstep_label)
        statstep_hbox.addWidget(self.statstep_text)

        image_hbox = QHBoxLayout()
        image_hbox.addWidget(self.image_button)
        image_hbox.addWidget(self.image_text)
        image_hbox.addWidget(self.image_choose)

        script_hbox = QHBoxLayout()
        script_hbox.addWidget(self.script_button)
        script_hbox.addWidget(self.script_text)
        script_hbox.addWidget(self.script_choose)

        self.right_vbox = QVBoxLayout()
        self.pluginVbox = QVBoxLayout() # place plugins can put their UI elts
        self.right_vbox.addWidget(self.experiment_label)
        self.right_vbox.addWidget(self.canvas)
        self.right_vbox.addLayout(hbox)
        self.right_vbox.addLayout(framerate_hbox)
        self.right_vbox.addLayout(statstep_hbox)
        self.right_vbox.addLayout(script_hbox)
        self.right_vbox.addLayout(image_hbox)
        self.right_vbox.addLayout(self.button_layout)
        self.right_vbox.addLayout(self.pluginVbox)
        
        self.left_vbox = QVBoxLayout()
        self.left_vbox.addWidget(self.expQList)
        self.left_vbox.addWidget(self.refresh_expQList_button)
        
        self.hbox = QHBoxLayout()
        self.hbox.addLayout(self.left_vbox)
        self.hbox.addLayout(self.right_vbox)
        
        self.main_frame.setLayout(self.hbox)
        self.setCentralWidget(self.main_frame)
        
        # UI setup is complete, now load in the experiment
        self.ui_chose_experiment(self.expQList.currentItem())
        return

if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = AppForm()
    form.show()
     #return app
    app.exec_() 
