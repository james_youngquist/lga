lga = form.experiment.lga

print "LGA time is", lga.t

def print_hi():
    print "Hello World!"

try:
    form.addButton('say_hi', 'Hello', print_hi)
except AttributeError:
    print "That button has already been added"

print "To load an image from a script, use these commands:"
print "lga.loadImage('/tmp/particles2.png')"
print "form.ui_redraw()"
